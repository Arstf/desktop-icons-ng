# Swedish translation for ding.
# Copyright © 2018-2021 ding's COPYRIGHT HOLDER
# This file is distributed under the same license as the ding package.
# Anders Jonsson <anders.jonsson@norsjovallen.se>, 2018-2021.
# Josef Andersson <l10nl18nsweja@gmail.com>, 2019.
# Luna Jernberg <droidbittin@gmail.com>, 2021. 
#
msgid ""
msgstr ""
"Project-Id-Version: ding\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-06-18 22:06+0200\n"
"PO-Revision-Date: 2021-04-07 17:52+0000\n"
"Last-Translator: Anders Jonsson <Unknown>\n"
"Language-Team: Swedish <tp-sv@listor.tp-sv.se>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: askConfirmPopup.js:36 askNamePopup.js:37 desktopIconsUtil.js:211
#: desktopManager.js:1481
msgid "Cancel"
msgstr "Avbryt"

#: askConfirmPopup.js:37
msgid "Delete"
msgstr "Ta bort"

#: askNamePopup.js:36
msgid "OK"
msgstr "OK"

#: askRenamePopup.js:40
msgid "Folder name"
msgstr "Mappnamn"

#: askRenamePopup.js:40
msgid "File name"
msgstr "Filnamn"

#: askRenamePopup.js:47
msgid "Rename"
msgstr "Byt namn"

#: desktopIconsUtil.js:80
msgid "Command not found"
msgstr "Kommandot hittades inte"

#: desktopIconsUtil.js:202
msgid "Do you want to run “{0}”, or display its contents?"
msgstr "Vill du köra ”{0}”, eller visa dess innehåll?"

#: desktopIconsUtil.js:203
msgid "“{0}” is an executable text file."
msgstr "”{0}” är en körbar textfil."

#: desktopIconsUtil.js:207
msgid "Execute in a terminal"
msgstr "Kör i en terminal"

#: desktopIconsUtil.js:209
msgid "Show"
msgstr "Visa"

#: desktopIconsUtil.js:213
msgid "Execute"
msgstr "Kör"

#: desktopManager.js:135
msgid "Nautilus File Manager not found"
msgstr "Nautilus filhanterare hittades inte"

#: desktopManager.js:136
msgid "The Nautilus File Manager is mandatory to work with Desktop Icons NG."
msgstr ""
"Filhanteraren Nautilus är obligatorisk för att arbeta med Desktop Icons NG."

#: desktopManager.js:563
msgid "New Folder"
msgstr "Ny mapp"

#: desktopManager.js:567
msgid "New Document"
msgstr "Nytt dokument"

#: desktopManager.js:572
msgid "Paste"
msgstr "Klistra in"

#: desktopManager.js:576
msgid "Undo"
msgstr "Ångra"

#: desktopManager.js:580
msgid "Redo"
msgstr "Gör om"

#: desktopManager.js:586
msgid "Select all"
msgstr "Markera alla"

#: desktopManager.js:592
msgid "Show Desktop in Files"
msgstr "Visa skrivbord i Filer"

#: desktopManager.js:596 fileItem.js:913
msgid "Open in Terminal"
msgstr "Öppna i terminal"

#: desktopManager.js:602
msgid "Change Background…"
msgstr "Ändra bakgrund…"

#: desktopManager.js:611
msgid "Display Settings"
msgstr "Visningsinställningar"

#: desktopManager.js:618
msgid "Desktop Icons settings"
msgstr ""

#: desktopManager.js:629
msgid "Scripts"
msgstr "Skript"

#: desktopManager.js:1148 desktopManager.js:1196
msgid "Error while deleting files"
msgstr "Fel vid borttagning av filer"

#: desktopManager.js:1222
msgid "Are you sure you want to permanently delete these items?"
msgstr "Är du säker på att du vill ta bort dessa objekt permanent?"

#: desktopManager.js:1223
msgid "If you delete an item, it will be permanently lost."
msgstr "Om du tar bort ett objekt är det borta för alltid."

#: desktopManager.js:1339
msgid "New folder"
msgstr "Ny mapp"

#: desktopManager.js:1414
msgid "Can not email a Directory"
msgstr ""

#: desktopManager.js:1415
msgid "Selection includes a Directory, compress the directory to a file first."
msgstr ""

#: desktopManager.js:1479
msgid "Select Extract Destination"
msgstr ""

#: desktopManager.js:1482
#, fuzzy
msgid "Select"
msgstr "Markera alla"

#. TRANSLATORS: "Home" is the text that will be shown in the user's personal folder
#: fileItem.js:153
msgid "Home"
msgstr "Hem"

#: fileItem.js:809
msgid "Open All..."
msgstr "Öppna alla…"

#: fileItem.js:809
msgid "Open"
msgstr "Öppna"

#: fileItem.js:816
msgid "Open All With Other Application..."
msgstr "Öppna alla med annat program…"

#: fileItem.js:816
msgid "Open With Other Application"
msgstr "Öppna med annat program"

#: fileItem.js:820
msgid "Launch using Dedicated Graphics Card"
msgstr "Starta med dedikerat grafikkort"

#: fileItem.js:828
msgid "Cut"
msgstr "Klipp ut"

#: fileItem.js:831
msgid "Copy"
msgstr "Kopiera"

#: fileItem.js:835
msgid "Rename…"
msgstr "Byt namn…"

#: fileItem.js:839
msgid "Move to Trash"
msgstr "Flytta till papperskorgen"

#: fileItem.js:843
msgid "Delete permanently"
msgstr "Ta bort permanent"

#: fileItem.js:849
msgid "Don't Allow Launching"
msgstr "Tillåt ej programstart"

#: fileItem.js:849
msgid "Allow Launching"
msgstr "Tillåt programstart"

#: fileItem.js:856
msgid "Empty Trash"
msgstr "Töm papperskorgen"

#: fileItem.js:863
msgid "Eject"
msgstr "Mata ut"

#: fileItem.js:870
msgid "Unmount"
msgstr "Avmontera"

#: fileItem.js:885
msgid "Extract Here"
msgstr "Extrahera här"

#: fileItem.js:888
msgid "Extract To..."
msgstr "Extrahera till…"

#: fileItem.js:893
msgid "Send to..."
msgstr "Skicka till…"

#: fileItem.js:897
msgid "Compress {0} file"
msgid_plural "Compress {0} files"
msgstr[0] "Komprimera {0} fil"
msgstr[1] "Komprimera {0} filer"

#: fileItem.js:900
msgid "New Folder with {0} item"
msgid_plural "New Folder with {0} items"
msgstr[0] "Ny mapp med {0} objekt"
msgstr[1] "Ny mapp med {0} objekt"

#: fileItem.js:905
msgid "Common Properties"
msgstr "Gemensamma egenskaper"

#: fileItem.js:905
msgid "Properties"
msgstr "Egenskaper"

#: fileItem.js:909
msgid "Show All in Files"
msgstr "Visa alla i Filer"

#: fileItem.js:909
msgid "Show in Files"
msgstr "Visa i Filer"

#: preferences.js:91
msgid "Settings"
msgstr "Inställningar"

#: preferences.js:98
msgid "Size for the desktop icons"
msgstr "Storlek för skrivbordsikonerna"

#: preferences.js:98
msgid "Tiny"
msgstr "Mycket liten"

#: preferences.js:98
msgid "Small"
msgstr "Liten"

#: preferences.js:98
msgid "Standard"
msgstr "Standard"

#: preferences.js:98
msgid "Large"
msgstr "Stor"

# TODO: *ON* the desktop?
#: preferences.js:99
msgid "Show the personal folder in the desktop"
msgstr "Visa den personliga mappen på skrivbordet"

# TODO: *ON* the desktop?
#: preferences.js:100
msgid "Show the trash icon in the desktop"
msgstr "Visa papperskorgsikonen på skrivbordet"

#: preferences.js:101 schemas/org.gnome.shell.extensions.ding.gschema.xml:38
msgid "Show external drives in the desktop"
msgstr "Visa externa enheter på skrivbordet"

#: preferences.js:102 schemas/org.gnome.shell.extensions.ding.gschema.xml:43
msgid "Show network drives in the desktop"
msgstr "Visa nätverksenheter på skrivbordet"

#: preferences.js:105
msgid "New icons alignment"
msgstr "Justering för nya ikoner"

#: preferences.js:106
msgid "Top-left corner"
msgstr "Övre vänstra hörnet"

#: preferences.js:107
msgid "Top-right corner"
msgstr "Övre högra hörnet"

#: preferences.js:108
msgid "Bottom-left corner"
msgstr "Nedre vänstra hörnet"

#: preferences.js:109
msgid "Bottom-right corner"
msgstr "Nedre högra hörnet"

#: preferences.js:111 schemas/org.gnome.shell.extensions.ding.gschema.xml:48
msgid "Add new drives to the opposite side of the screen"
msgstr "Lägg till nya enheter på motsatt sida av skärmen"

#: preferences.js:112
msgid "Highlight the drop place during Drag'n'Drop"
msgstr "Markera släpplatsen under dra-och-släpp"

#: preferences.js:116
msgid "Settings shared with Nautilus"
msgstr "Inställningar delade med Nautilus"

#: preferences.js:122
msgid "Click type for open files"
msgstr "Klicktyp för filöppning"

#: preferences.js:122
msgid "Single click"
msgstr "Enkelklick"

#: preferences.js:122
msgid "Double click"
msgstr "Dubbelklick"

#: preferences.js:123
msgid "Show hidden files"
msgstr "Visa dolda filer"

#: preferences.js:124
msgid "Show a context menu item to delete permanently"
msgstr "Visa ett snabbvalsmenyobjekt för att ta bort permanent"

#: preferences.js:129
msgid "Action to do when launching a program from the desktop"
msgstr "Åtgärd att utföra när ett program startas från skrivbordet"

#: preferences.js:130
msgid "Display the content of the file"
msgstr "Visa innehållet i filen"

#: preferences.js:131
msgid "Launch the file"
msgstr "Kör filen"

#: preferences.js:132
msgid "Ask what to do"
msgstr "Fråga vad som ska göras"

#: preferences.js:138
msgid "Show image thumbnails"
msgstr "Visa miniatyrbilder"

#: preferences.js:139
msgid "Never"
msgstr "Aldrig"

#: preferences.js:140
msgid "Local files only"
msgstr "Endast lokala filer"

#: preferences.js:141
msgid "Always"
msgstr "Alltid"

#: prefs.js:37
#, fuzzy
msgid ""
"To configure Desktop Icons NG, do right-click in the desktop and choose the "
"last item: 'Desktop Icons settings'"
msgstr ""
"För att konfigurera Desktop Icons NG, högerklicka på skrivbordet och välj "
"det sista objektet: ”Inställningar”"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:18
msgid "Icon size"
msgstr "Ikonstorlek"

# TODO: *ON* the desktop?
#: schemas/org.gnome.shell.extensions.ding.gschema.xml:19
msgid "Set the size for the desktop icons."
msgstr "Ställ in storleken för skrivbordsikonerna."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:23
msgid "Show personal folder"
msgstr "Visa personlig mapp"

# TODO: *ON* the desktop?
#: schemas/org.gnome.shell.extensions.ding.gschema.xml:24
msgid "Show the personal folder in the desktop."
msgstr "Visa den personliga mappen på skrivbordet."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:28
msgid "Show trash icon"
msgstr "Visa papperskorgsikon"

# TODO: *ON* the desktop?
#: schemas/org.gnome.shell.extensions.ding.gschema.xml:29
msgid "Show the trash icon in the desktop."
msgstr "Visa papperskorgsikonen på skrivbordet."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:33
msgid "New icons start corner"
msgstr "Starthörn för nya ikoner"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:34
msgid "Set the corner from where the icons will start to be placed."
msgstr "Ställ in hörnet där ikonerna ska börja placeras."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:39
msgid "Show the disk drives connected to the computer."
msgstr "Visa de hårddiskar som är anslutna till datorn."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:44
msgid "Show mounted network volumes in the desktop."
msgstr "Visa monterade nätverksvolymer på skrivbordet."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:49
msgid ""
"When adding drives and volumes to the desktop, add them to the opposite side "
"of the screen."
msgstr ""
"När enheter och volymer läggs till på skrivbordet, lägg till dem på motsatt "
"sida av skärmen."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:53
msgid "Shows a rectangle in the destination place during DnD"
msgstr "Visar en rektangel i målplatsen under dra-och-släpp"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:54
msgid ""
"When doing a Drag'n'Drop operation, marks the place in the grid where the "
"icon will be put with a semitransparent rectangle."
msgstr ""
"När en dra-och-släpp-operation utförs, markeras platsen i rutnätet där "
"ikonen kommer placeras med en halvgenomskinlig rektangel."
